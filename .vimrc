" Joe Borrello's Long Overdue VimRC File
colorscheme delek
set number

set so=7 "Apperently the cursor gets 7 lines.... no idea

set nocompatible
filetype off
filetype plugin on
syntax on

set rtp+=~/vimrc/bundle/Vundle.vim
call vundle#begin()

" Actual Vundle
Plugin 'VundleVim/Vundle.vim'

" Nerdtree Plugin
Plugin 'scrooloose/nerdtree'

"VimLatex Plugin
Plugin 'vim-latex/vim-latex'

" Autocompletion
" Plugin 'Valloric/YouCompleteMe'

" VimWiki Plugin
Plugin 'vimwiki/vimwiki'

call vundle#end()
filetype plugin indent on

" Nerd Tree Commands
"autocmd vimenter * NERDTree
map <C-n> :NERDTreeToggle<CR>

map <C-T> :tabnew<CR>
map <C-Left> :tabp<CR>
map <C-Right> :tabn<CR>
