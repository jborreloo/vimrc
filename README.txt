This is the repo for all vim-related code.

Vim packages via Vundle actually go in the ~/vim directory. These updates are then called in the .vimrc file.
^this was updated, it used to be the .vim directory, but that has since been changed to reflect the ability to update the bundles via git as well

In order to keep the .vimrc file updated across systems, though, this folder (~/vim) is used as a butbucket repo.

As the needs of the .vimrc file change, the repo will change

So it goes like this:
-.vimrc is updated here in ~/vim, and is linked to the home directory
-~/vim is where the packages to update vim go (nowhere else)
-~/vim is the local directory that is linked to the bitbucket repo
